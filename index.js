const express = require('express');
const Parser = require('rss-parser');
const mongoose = require('mongoose');

const News = require('./model/news');

const app = express();
mongoose.connect('mongodb://localhost:27017/NewsFeed', {useNewUrlParser: true});
app.use(express.json());

app.get('/',async (req,res,next)=>{
  let parser = new Parser();
  let resfeed = await parser.parseURL('https://www.reddit.com/.rss');
  let feed = resfeed.items;
  let sortedFeed = feed.sort((a,b)=> new Date(a.pubDate).getTime() -new Date(b.pubDate).getTime());  
  News.findOne().sort({$natural: -1}).limit(1).exec(function(err, firstNews){
    if(err) throw new Error;
    if(!firstNews) {
            News.insertMany(sortedFeed, function(error, docs) {
              if(error) throw new Error
              res.status(200).send(`${docs.length} new record got inserted`);
            });
    }
     else{
            let results =  sortedFeed.filter((d) => {
                  return new Date(d.pubDate).getTime() > new Date(firstNews.pubDate).getTime();
                });  
            if(results.length === 0)  res.status(201).send('no new record to insert');
            else{
                  try {
                    News.insertMany(results, function(error, docs) {
                      if(error) throw new Error
                      res.status(200).send(`${docs.length} new record got inserted`);
                    });
                  }
                  catch (e) {
                    throw Error(e);
                  }
              }     
        }
    
  });   
});

app.post((err,req,res,next)=>{
    res.status(422).send('something went wrong');
  });

app.listen(3000,()=>{ console.log('listning to post 3000')});
  