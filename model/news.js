const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newsSchema = new Schema({
    title:String,
    link:String,
    pubDate:Date,
    author:String,
    content:String,
    isoDate:Date
})

const Product = mongoose.model('news', newsSchema);
module.exports = Product;